// tslint:disable
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';



@Injectable()
export class FuncionesService {

  constructor(private router: Router) { }



  getUsuarioLogueado(){
    return {
      nombre: localStorage.getItem("musical_nombre"),
      id: localStorage.getItem("musical_id"),
      nivel: localStorage.getItem("musical_nivel")
    }
  }




  ponerCeros(num:number, tamanio:number): string {
    let minumero = String(num);
    while (minumero.length < tamanio) {
      minumero = '0' + minumero;
    }
    return minumero;
  }

  redondear(numero: number, decimales?: number): number {
    let retorno = numero;
    let cad = String(numero);
    console.info("Numero: " + cad);
    console.warn("Truncado: " + cad.substr(0, 6)); //6.0000
    let num = parseFloat(cad).toPrecision(7);

    var newnumber = new Number(numero + '').toFixed(2);
    return parseFloat(newnumber);
  }

  fecha_y_hora_actual() {
    let ahora = new Date();
    let mes = this.ponerCeros((ahora.getMonth() + 1), 2);
    let fecha = ahora.getFullYear() + "-" + mes + "-" + ahora.getDate();
    let hora = this.ponerCeros(ahora.getHours(), 2) + ":" + this.ponerCeros(ahora.getMinutes(), 2) + ":" + this.ponerCeros(ahora.getSeconds(), 2);
    return {fecha: fecha, hora: hora};
  }



}
