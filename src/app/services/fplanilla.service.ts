import { Injectable } from '@angular/core';
import { FmysqlService } from './fmysql.service';
import { FuncionesService } from './funciones.service';
import { Planillas } from '../interfaces/planillas';
import { environment } from 'src/environments/environment';
import { respuestaAPI, formatoGuardar } from '../interfaces/estructuras';
import { DetPlanilla } from '../interfaces/det_planilla';

@Injectable({
  providedIn: 'root'
})
export class FplanillaService {

  constructor(private fmysql: FmysqlService,private funciones:FuncionesService) { }

  _get_nro_planilla() {
    let promesa = new Promise((resolve, reject) => {
      this.fmysql.listar_api(environment.api_maxplanilla).subscribe(snap => {
        let res: respuestaAPI = snap.json();
        console.log('Nro. Max. de planilla:', res);
        if (res.ok) {
          if (!res.data[0].max) {
            resolve(1);
          } else {
            let nromax = Number(res.data[0].max) + 1;
            resolve(nromax);
          }
        } else {
          reject(null);
        }
      });
    });
    return promesa;
  }

  _guardar_planilla(planilla: Planillas) {
    let promesa = new Promise((resolve, reject) => {
      let item: formatoGuardar = {
        datos: [
          planilla.idplanilla,
          planilla.idusuario,
          planilla.idartista,
          planilla.nic,
          planilla.pais,
          planilla.ciudad,
          planilla.fecha,
          planilla.nombre_evento,
          planilla.lugar_evento,
          planilla.responsable,
          planilla.telefono_responsable,
          planilla.direccion_responsable,
          planilla.descripcion,
          planilla.vigencia,
          planilla.creacion,
          planilla.modificacion
        ]
      };
      this.fmysql.registrar(environment.api_planillas, item).subscribe(snap => {
        console.log('Registro de planilla:', snap);
        let res: respuestaAPI = snap.json();
        if (res.ok) {
          resolve(res.id);
        } else {
          reject(null);
        }
      });
    });
    return promesa;
  }

  _guardar_detalle_planilla(det: DetPlanilla) {
    let promesa = new Promise((resolve, reject) => {
      let item: formatoGuardar = {
        datos: [
          det.iddetplanilla,
          det.idplanilla,
          det.tema,
          det.autor,
          det.veces,
          det.otros,
          det.vigencia,
        ]
      };
      this.fmysql.registrar(environment.api_det_planilla, item).subscribe(snap => {
        let res: respuestaAPI = snap.json();
        console.log('Registro de Detalle en planilla:', res);
        if (res.ok) {
          resolve(res.id);
        } else {
          reject(null);
        }
      });
    });
    return promesa;
  }
}
