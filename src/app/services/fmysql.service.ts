// tslint:disable
import { Injectable } from '@angular/core';
import { formatoEjecutar, formatoGuardar, formatoModificacion } from '../interfaces/estructuras';
import { Http, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class FmysqlService {

  constructor(private http: Http, private httpCliente: HttpClient) { }

  listar_api(urlapi: string) {
    let url = urlapi;
    console.warn("URL API: [" + url + "]");
    return this.http.get(url);
  }

  listar(urlapi: string) {
    let url = urlapi + "listar/";
    console.warn("URL API: [" + url + "]");
    return this.http.get(url);
  }

  buscar_id(urlapi: string, id: number) {
    let url = urlapi + "buscar/" + id;
    console.warn("URL API: [" + url + "]");
    return this.http.get(url);
  }

  buscar_campo(urlapi: string, campo: string, valor: string | number) {
    let url = urlapi + "buscar/" + campo + "/" + valor;
    console.warn("URL API: [" + url + "]");
    return this.http.get(url);
  }

  registrar(urlapi: string, data: formatoGuardar) {
    let url = urlapi + "guardar/";
    console.warn("URL API: [" + url + "]");
    return this.http.post(url, data);
  }

  subir_imagen_old(urlapi: string, data: File) {
    let url = urlapi;
    console.warn("URL API: [" + url + "]");
    console.log(data);
    const formData: FormData = new FormData();
    formData.append('file', data);
    //.post(url, formData, { headers: yourHeadersConfig })
    let headers: Headers = new Headers();
    headers.set("Content-Type", undefined);
    headers.append('Process-Data', 'false');
    return this.http.post(url, formData, { headers: headers });
  }

  subir_imagen(urlapi: string, file: File) {
    let url = urlapi;
    console.warn(file);
    const formData = new FormData();
    formData.append('file', file);
    const headers = new HttpHeaders().set('Content-Type', []);
    return this.httpCliente.post(url, formData, {
      headers,
    });
  }

  modificar(urlapi: string, data: formatoModificacion) {
    let url = urlapi + "modificar/";
    console.warn("URL API: [" + url + "]");
    return this.http.put(url, data);
  }

  ejecutar(urlapi: string, data: formatoEjecutar) {
    let url = urlapi + "sql/";
    console.warn("URL API: [" + url + "]");

    let headers: Headers = new Headers();
    headers.set("Access-Control-Allow-Origin", "*");
    headers.append("Access-Control-Allow-Methods", "GET, PUT, POST");

    /*
    res.header();
    res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST");*/

    return this.http.post(url, data, { headers: headers });
  }

  borrar(urlapi: string, id: number) {
    let url = urlapi + "borrar/" + id;
    console.warn("URL API: [" + url + "]");
    return this.http.delete(url);
  }
}
