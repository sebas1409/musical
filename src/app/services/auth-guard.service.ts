// tslint:disable
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { FuncionesService } from './funciones.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router,private funcion:FuncionesService) { }

  canActivate(): boolean {

    let autorizacion_admin =this.funcion.getUsuarioLogueado().id;


    //console.log("Autorizacion admin? " + autorizacion_admin);
    if (autorizacion_admin != null) {
      return true;
    }
    this.router.navigate(['/acceso']);
    return false;

  }


}
