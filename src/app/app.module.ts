import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './app.route';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { DashModule } from './dash/dash.module';



import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';



import { FmysqlService } from './services/fmysql.service';
import { HttpClientModule } from '@angular/common/http';
import { ControlesService } from './services/controles.service';
import { FuncionesService } from './services/funciones.service';
import { MisPipesModule } from './pipes/mis-pipes.module';
import { RefrescarComponent } from './refrescar/refrescar.component';



// In your App's module:

@NgModule({
  declarations: [
    AppComponent,
    RefrescarComponent


  ],
  imports: [
    BrowserModule, RouterModule.forRoot(routes),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    MaterialModule,
    DashModule,
    HttpModule,
    FormsModule,
    SharedModule,
    AuthenticationModule,
    HttpClientModule,
    MisPipesModule

  ],
  providers: [FmysqlService,ControlesService,FuncionesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
