import { Routes } from '@angular/router';
import { DashComponent } from './dash/dash.component';
import { AuthenticationComponent} from './authentication/authentication.component';
import { ResetPasswordComponent} from './authentication/reset-password/reset-password.component';
import { AdminComponent } from './dash/admin/admin.component';
import { AuthGuardService } from './services/auth-guard.service';


import { DashInicioComponent } from './dash/admin/dash-inicio/dash-inicio.component';


import { UsuariosComponent } from './dash/admin/usuarios/usuarios.component';
import { RegistroUsuariosComponent } from './dash/admin/usuarios/registro-usuarios/registro-usuarios.component';
import { EdicionUsuariosComponent } from './dash/admin/usuarios/edicion-usuarios/edicion-usuarios.component';
import { ArtistasComponent } from './dash/admin/artistas/artistas.component';
import { RegistroArtistasComponent } from './dash/admin/artistas/registro-artistas/registro-artistas.component';
import { EdicionArtistasComponent } from './dash/admin/artistas/edicion-artistas/edicion-artistas.component';
import { PlanillasComponent } from './dash/admin/planillas/planillas.component';
import { RegistroPlanillasComponent } from './dash/admin/planillas/registro-planillas/registro-planillas.component';
import { EdicionPlanillasComponent } from './dash/admin/planillas/edicion-planillas/edicion-planillas.component';
import { PlanillaImpresionComponent } from './dash/admin/planillas/planilla-impresion/planilla-impresion.component';
import { PlanillasUsuarioComponent } from './dash/admin/planillas-usuario/planillas-usuario.component';
import { ConfiguracionComponent } from './dash/admin/configuracion/configuracion.component';







export const routes: Routes = [


 // { path: '', redirectTo:'web', pathMatch:'full'},
 { path: '', redirectTo:'acceso', pathMatch:'full'},

  { path: 'acceso', component: AuthenticationComponent },
  { path: 'cambiar-clave', component: ResetPasswordComponent},




  {path: 'dash', component: AdminComponent,canActivate:[AuthGuardService],children: [
    { path: '', component: DashInicioComponent },





    { path: 'inicio', component: DashInicioComponent },

    { path: 'usuarios', component: UsuariosComponent },
    { path: 'usuarios/registrar', component: RegistroUsuariosComponent },
    { path: 'usuarios/editar/:id', component: EdicionUsuariosComponent },

    { path: 'artistas', component: ArtistasComponent },
    { path: 'artistas/registrar', component: RegistroArtistasComponent },
    { path: 'artistas/editar/:id', component: EdicionArtistasComponent },

    { path: 'planillas', component: PlanillasComponent },
    { path: 'planillas/registrar', component: RegistroPlanillasComponent },
    { path: 'planillas/editar/:id', component: EdicionPlanillasComponent },
    { path: 'planillas/impresion/:id', component: PlanillaImpresionComponent },


    { path: 'planilla', component: PlanillasUsuarioComponent },
    { path: 'planilla/registrar', component: RegistroPlanillasComponent },
    { path: 'planilla/editar/:id', component: EdicionPlanillasComponent },
    { path: 'planilla/impresion/:id', component: PlanillaImpresionComponent },

    { path: 'configuracion/videos', component: ConfiguracionComponent },








    { path: '**', component: DashInicioComponent},


   ]},
  { path: '**', component: ResetPasswordComponent},
];
