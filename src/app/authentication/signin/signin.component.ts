import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { MatSnackBar } from '@angular/material';
import { ControlesService } from 'src/app/services/controles.service';
import { FuncionesService } from 'src/app/services/funciones.service';
import { respuestaAPI } from 'src/app/interfaces/estructuras';
import { FmysqlService } from 'src/app/services/fmysql.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  login = { usuario: null, clave: null };
  usuario = this.controles._requerido();
  clave = this.controles._letras_con_numero();
  hide: boolean = true;
  constructor(private _router: Router, private http: Http, public snackBar: MatSnackBar,
    private fmysql: FmysqlService, private controles: ControlesService,
    private funcion: FuncionesService) { }

  ngOnInit() { }

  loguear() {
    let url = environment.api_login + this.login.usuario + "/" + this.login.clave;
    this.fmysql.listar_api(url).subscribe(snap => {
      console.log(snap);


      if (snap.json().data.length > 0) {

        let res: respuestaAPI = snap.json();
        let acceso = res.data[0];
        localStorage.setItem("musical_id",acceso.idusuario);
        localStorage.setItem("musical_nivel",acceso.nivel);

        this.snackBar.open('Usuario','correcto!!!', {
          horizontalPosition: 'right', duration: 3000
        });
        // this.alerta.acceso_correcto();
        //this.buscar_contacto(acceso.idusuario);
        this.funcion.getUsuarioLogueado().id;
        this.funcion.getUsuarioLogueado().nivel;
        this._router.navigate(["/dash"]);

      } else {
        console.log("Usuario Erroneo");
        this.snackBar.open('Usuario','incorrecto!!!', {
          horizontalPosition: 'right', duration: 3000
        });;
      }
    });
  }

  buscar_contacto(idusuario) {
    this.fmysql.buscar_id(environment.api_usuarios, idusuario).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      let es_admin = res.data[0].es_admin;
      let es_profesional = res.data[0].es_profesional;
      let es_deportista = res.data[0].es_deportista;
      let es_centrosalud = res.data[0].es_centrosalud;
      let es_instdeportiva = res.data[0].es_instdeportiva;


      if (es_admin == 1) {
        //this.funcion.set_id_admin(idusuario);
        this._router.navigate(["/dash/admin"]);
      } else if (es_profesional == 1 || es_deportista == 1 || es_centrosalud == 1 || es_instdeportiva == 1) {
       // this.funcion.set_id_usuario(idusuario);
        this._router.navigate(["/dash/usuario"]);

      } else {
        alert("El acceso fue denegado, no existe el usuario y/o contraseña");
      }

    });
  }

}
