import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SigninComponent } from './signin/signin.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AuthenticationComponent } from './authentication.component';
import { MaterialModule} from '../material';
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [
    CommonModule,MaterialModule,FormsModule, ReactiveFormsModule, RouterModule
  ],
  declarations: [AuthenticationComponent,SigninComponent,ResetPasswordComponent]
})
export class AuthenticationModule { }
