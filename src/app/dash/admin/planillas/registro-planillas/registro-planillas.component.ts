import { Component, OnInit } from '@angular/core';
import { Planillas } from 'src/app/interfaces/planillas';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { ListadoDeclaracionesComponent } from 'src/app/modales/listado-declaraciones/listado-declaraciones.component';
import { DetPlanilla } from 'src/app/interfaces/det_planilla';
import { FplanillaService } from 'src/app/services/fplanilla.service';
import { ListadoArtistasComponent } from 'src/app/modales/listado-artistas/listado-artistas.component';
import { Router } from '@angular/router';
import { FuncionesService } from 'src/app/services/funciones.service';

@Component({
  selector: 'app-registro-planillas',
  templateUrl: './registro-planillas.component.html',
  styleUrls: ['./registro-planillas.component.scss']
})
export class RegistroPlanillasComponent implements OnInit {

  busqueda;
  public data: any[];
  public rowsOnPage = 5;
  item: Planillas = {
    idplanilla: null,
    idusuario:this.funcion.getUsuarioLogueado().id,
    idartista: null,
    nic:null,
    pais: null,
    ciudad: null,
    fecha: this.funcion.fecha_y_hora_actual().fecha,
    nombre_evento: null,
    lugar_evento: null,
    responsable: null,
    telefono_responsable:null,
    direccion_responsable:null,
    descripcion:null,
    vigencia: 1,
    creacion: 'now()',
    modificacion: null
  }
  nro_planilla;
  det_planilla:DetPlanilla={
      iddetplanilla:null,
      idplanilla:null,
      tema:null,
      autor:null,
      veces:null,
      otros:null,
      vigencia:1
  }
  detalle_venta= [];
  artista;
  nivel:any=this.funcion.getUsuarioLogueado().nivel;
  constructor(public dialog: MatDialog,private fmysql: FmysqlService,private fplanilla:FplanillaService,private snackBar:MatSnackBar,private _route:Router,private funcion:FuncionesService) { }

  ngOnInit(){
    this.buscar_nro_venta();
  }

  buscar_nro_venta() {
    this.fplanilla._get_nro_planilla().then(res => {
      let nro = Number(res);
      let codigo = 'N° - ' + this.funcion.ponerCeros(nro, 6);
      this.nro_planilla = codigo;
    });
  }

  buscar_artista(editar: boolean, item?: any) {
    const dialogRef = this.dialog.open(ListadoArtistasComponent, {
      panelClass: ['panel-modal', 'panel-modal-60'],
      data: { editar: editar, item: item }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("resultado-->", result);
      if (result) {
       this.item.idartista=result.idartista;
       this.artista=result.nombre;

       console.log(this.item.idartista,"-asignacion artista->",this.artista)
      }
    });
  }

  buscar_listado_declaraciones(editar: boolean, item?: any) {
    const dialogRef = this.dialog.open(ListadoDeclaracionesComponent, {
      panelClass: ['panel-modal', 'panel-modal-60'],
      data: { editar: editar, item: item }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("resultado-->", result);
      if (result) {
       this.asignar_tabla(result);
      }
    });
  }

  asignar_tabla(result){
      this.det_planilla={
        iddetplanilla:null,
        idplanilla:null,
        tema:result.nombre,
        autor:result.autor,
        veces:null,
        otros:result.genero,
        vigencia:1
      }
    this.detalle_venta.push(this.det_planilla);

  }

  quitar_producto_detalle(ind) {
    this.detalle_venta.splice(ind, 1);
  }


  guardar_planilla() {
    let verificacion = 1;
    this.fplanilla._guardar_planilla(this.item).then(idplanilla => {

      this.detalle_venta.forEach(prod => {
          prod.idplanilla=Number(idplanilla);
        this.fplanilla._guardar_detalle_planilla(prod).then(idprodet => {
          let det: DetPlanilla = {
            iddetplanilla: null, idplanilla: Number(idplanilla), vigencia: 1
          };

            console.log(verificacion + '/' + this.detalle_venta.length);
            if (verificacion == this.detalle_venta.length) {
              this.snackBar.open('Se realizo la planilla', 'Satisfactoriamente!!', {
                horizontalPosition: 'right', duration: 3000
              });
              /*let url = window.location.href;
              setTimeout(() => {
                window.open(url + '/' + id_venta, '_blank');
              }, 300);*/

            if(this.nivel==1){
              this._route.navigate(["/dash/planillas"]);
            }else{
              this._route.navigate(["/dash/planilla"]);

            }

            } else {
              verificacion++;
            }

        });
      });
    });
  }

}
