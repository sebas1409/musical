import { Component, OnInit } from '@angular/core';
import { Planillas } from 'src/app/interfaces/planillas';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { DetPlanilla } from 'src/app/interfaces/det_planilla';
import { FuncionesService } from 'src/app/services/funciones.service';
import { ListadoDeclaracionesComponent } from 'src/app/modales/listado-declaraciones/listado-declaraciones.component';
import { ListadoArtistasComponent } from 'src/app/modales/listado-artistas/listado-artistas.component';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { respuestaAPI, formatoModificacion } from 'src/app/interfaces/estructuras';
import { FplanillaService } from 'src/app/services/fplanilla.service';

@Component({
  selector: 'app-edicion-planillas',
  templateUrl: '../registro-planillas/registro-planillas.component.html',
  styleUrls: ['../registro-planillas/registro-planillas.component.scss']
})
export class EdicionPlanillasComponent implements OnInit {

  busqueda;
  public data: any[];
  public rowsOnPage = 5;
  item: Planillas = {
    idplanilla: null,
    idusuario:null,
    idartista: null,
    nic:null,
    pais: null,
    ciudad: null,
    fecha: this.funcion.fecha_y_hora_actual().fecha,
    nombre_evento: null,
    lugar_evento: null,
    responsable: null,
    telefono_responsable: null,
    direccion_responsable: null,
    descripcion: null,
    vigencia: 1,
    creacion: 'now()',
    modificacion: null
  }
  det_planilla: DetPlanilla = {
    iddetplanilla: null,
    idplanilla: null,
    tema: null,
    autor: null,
    veces: null,
    otros: null,
    vigencia: 1
  }
  detalle_venta = [];
  artista;
  nro_planilla;
  nivel:any=this.funcion.getUsuarioLogueado().nivel;
  constructor(public dialog: MatDialog, private fmysql: FmysqlService, private funcion: FuncionesService, private state: ActivatedRoute, private _route: Router,private fplanilla:FplanillaService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.item.idplanilla = this.state.snapshot.params.id;
    this.listar_planillas();

  }

  listar_planillas() {
    this.fmysql.buscar_id(environment.api_planillas, this.item.idplanilla).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.item = res.data[0];
      this.nro_planilla = 'N° - ' + this.funcion.ponerCeros(this.item.idplanilla, 6);
      this.listar_artista(res.data[0].idartista);
      this.listar_detalle_venta(res.data[0].idplanilla);

    });
  }
  listar_artista(idartista) {
    this.fmysql.buscar_id(environment.api_artistas, idartista).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      let item = res.data[0];
      this.artista = item.nombre;

    });
  }
  listar_detalle_venta(idplanilla) {


    this.fmysql.buscar_campo(environment.api_det_planilla, "idplanilla", idplanilla).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      console.log('Detalles:', res.data);
      this.detalle_venta = res.data;

    });
  }

  buscar_artista(editar: boolean, item?: any) {
    const dialogRef = this.dialog.open(ListadoArtistasComponent, {
      panelClass: ['panel-modal', 'panel-modal-60'],
      data: { editar: editar, item: item }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("resultado-->", result);
      if (result) {
        this.item.idartista = result.idartista;
        this.artista = result.nombre;

        console.log(this.item.idartista, "-asignacion artista->", this.artista)
      }
    });
  }

  buscar_listado_declaraciones(editar: boolean, item?: any) {
    const dialogRef = this.dialog.open(ListadoDeclaracionesComponent, {
      panelClass: ['panel-modal', 'panel-modal-60'],
      data: { editar: editar, item: item }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("resultado-->", result);
      if (result) {
        this.asignar_tabla(result);
      }
    });
  }





  guardar_planilla() {
    // this.btn_dashabilitado = true;
    let item_update: formatoModificacion = {
      idcampo: { nombre: "idplanilla", valor: this.item.idplanilla },
      campos:["idartista","nic", "pais", "ciudad",
        "fecha", "nombre_evento", "lugar_evento", "responsable", "telefono_responsable", "direccion_responsable", "descripcion", "modificacion"],
      valores: [this.item.idartista, this.item.nic,this.item.pais, this.item.ciudad,
      this.item.fecha, this.item.nombre_evento, this.item.lugar_evento, this.item.responsable, this.item.telefono_responsable, this.item.direccion_responsable, this.item.descripcion, 'now()']
    }
    this.fmysql.modificar(environment.api_planillas, item_update).subscribe(snap => {
      console.log("modificanco planilla", snap.json());
      let res: respuestaAPI = snap.json();
      if (res.ok) {
        this.actualizar_detalle_planilla();
      }
    });
  }

  actualizar_detalle_planilla() {
    this.detalle_venta.forEach((ele) => {
      let item_update: formatoModificacion = {

        idcampo: { nombre: "iddetplanilla", valor: ele.iddetplanilla },
        campos: ["idplanilla", "tema", "autor", "veces", "otros"],
        valores: [ele.idplanilla, ele.tema, ele.autor, ele.veces, ele.otros]
      }

      console.log("elemento-->",ele);
      if (ele.iddetplanilla) {
        this.fmysql.modificar(environment.api_det_planilla, item_update).subscribe(snap => {
          console.log("det_planilla-->", snap.json());
          let res: respuestaAPI = snap.json();
          if (res.ok) {
            //this._route.navigate(['dash/planillas']);
          }
        });
      }

      if(!ele.iddetplanilla && ele.idplanilla){
        let verificacion = 1;
        this.fplanilla._guardar_detalle_planilla(ele).then(idprodet => {


            console.log(verificacion + '/' + this.detalle_venta.length);
            if (verificacion == this.detalle_venta.length) {
              this.snackBar.open('Se realizo la planilla', 'Satisfactoriamente!!', {
                horizontalPosition: 'right', duration: 3000
              });


             // this._route.navigate(["/dash/planillas"]);

            } else {
              verificacion++;
            }

        });
      }

            if(this.nivel==1){
              this._route.navigate(["/dash/planillas"]);
            }else{
              this._route.navigate(["/dash/planilla"]);

            }
    });
  }

  asignar_tabla(result) {
    this.det_planilla = {
      iddetplanilla: null,
      idplanilla: this.item.idplanilla,
      tema: result.nombre,
      autor: result.autor,
      veces: null,
      otros: result.genero,
      vigencia: 1
    }
    this.detalle_venta.push(this.det_planilla);

  }

}
