import { Component, OnInit } from '@angular/core';
import { Planillas } from 'src/app/interfaces/planillas';
import { DetPlanilla } from 'src/app/interfaces/det_planilla';
import { ActivatedRoute } from '@angular/router';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { environment } from 'src/environments/environment';
import { respuestaAPI } from 'src/app/interfaces/estructuras';
import { FpdfService } from 'src/app/services/fpdf.service';
import { FuncionesService } from 'src/app/services/funciones.service';

@Component({
  selector: 'app-planilla-impresion',
  templateUrl: './planilla-impresion.component.html',
  styleUrls: ['./planilla-impresion.component.scss']
})
export class PlanillaImpresionComponent implements OnInit {
  busqueda;
  public data: any[];
  public rowsOnPage = 5;
  item: Planillas = {
    idplanilla: null,
    idartista: null,
    nic:null,
    pais: null,
    ciudad: null,
    fecha: null,
    nombre_evento: null,
    lugar_evento: null,
    responsable: null,
    telefono_responsable:null,
    direccion_responsable:null,
    descripcion:null,
    vigencia: 1,
    creacion: 'now()',
    modificacion: null
  }
  det_planilla:DetPlanilla={
      iddetplanilla:null,
      idplanilla:null,
      tema:null,
      autor:null,
      veces:null,
      otros:null,
      vigencia:null
  }
  detalle_venta= [];
  artista;
  nro_planilla;
  nivel:number=Number(this.funcion.getUsuarioLogueado().nivel);
  constructor(private state:ActivatedRoute,private fmysql:FmysqlService,private fpdf: FpdfService,private funcion:FuncionesService) { }

  ngOnInit() {
    this.item.idplanilla=this.state.snapshot.params.id;
    this.listar_planillas();
  }

  listar_artista(idartista){
    this.fmysql.buscar_id(environment.api_artistas,idartista).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      let item = res.data[0];
      this.artista=item.nombre;

    });
  }

  listar_planillas(){
    this.fmysql.buscar_id(environment.api_planillas,this.item.idplanilla).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.item = res.data[0];
      this.nro_planilla='N° - ' + this.funcion.ponerCeros(this.item.idplanilla, 6);
      this.listar_artista(res.data[0].idartista);
      this.listar_detalle_venta(res.data[0].idplanilla);

    });
  }

  listar_detalle_venta(idplanilla) {


    this.fmysql.buscar_campo(environment.api_det_planilla,"idplanilla",idplanilla).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      console.log('Detalles:', res.data);
      this.detalle_venta = res.data;

    });
  }

  exportar_a_pdf() {
    this.fpdf.exportar_a_pdf('area-exportacion');
  }

}
