import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatIconModule, MatSidenavModule, MatToolbarModule, MatSelectModule, MatOptionModule, MatAutocompleteModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material';
import { CdkTreeModule } from '@angular/cdk/tree';
import { DashInicioComponent } from './dash-inicio/dash-inicio.component';

import { UsuariosComponent } from './usuarios/usuarios.component';
import { RegistroUsuariosComponent } from './usuarios/registro-usuarios/registro-usuarios.component';
import { EdicionUsuariosComponent } from './usuarios/edicion-usuarios/edicion-usuarios.component';
import { MisPipesModule } from 'src/app/pipes/mis-pipes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ArtistasComponent } from './artistas/artistas.component';
import { RegistroArtistasComponent } from './artistas/registro-artistas/registro-artistas.component';
import { EdicionArtistasComponent } from './artistas/edicion-artistas/edicion-artistas.component';
import { DataTableModule } from 'angular-6-datatable';
import { CancionesComponent } from './canciones/canciones.component';
import { MisModalesModule } from 'src/app/modales/mis-modales.module';
import { PlanillasComponent } from './planillas/planillas.component';
import { RegistroPlanillasComponent } from './planillas/registro-planillas/registro-planillas.component';
import { EdicionPlanillasComponent } from './planillas/edicion-planillas/edicion-planillas.component';
import { PlanillaImpresionComponent } from './planillas/planilla-impresion/planilla-impresion.component';
import { PlanillasUsuarioComponent } from './planillas-usuario/planillas-usuario.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';


@NgModule({
  imports: [
    CommonModule,MatSidenavModule, FormsModule, BrowserAnimationsModule,
    MatToolbarModule, MatButtonModule, MatIconModule, RouterModule,MaterialModule,CdkTreeModule,MisModalesModule,
    MatSelectModule,MatOptionModule,MisPipesModule,SharedModule,DataTableModule,ReactiveFormsModule,MatAutocompleteModule
  ],
  declarations: [AdminComponent, DashInicioComponent, UsuariosComponent, RegistroUsuariosComponent, EdicionUsuariosComponent, ArtistasComponent, RegistroArtistasComponent, EdicionArtistasComponent, CancionesComponent, PlanillasComponent, RegistroPlanillasComponent, EdicionPlanillasComponent, PlanillaImpresionComponent, PlanillasUsuarioComponent, ConfiguracionComponent]
})
export class AdminModule { }
