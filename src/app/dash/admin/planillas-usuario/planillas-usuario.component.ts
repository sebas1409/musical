import { Component, OnInit } from '@angular/core';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { environment } from 'src/environments/environment';
import { respuestaAPI } from 'src/app/interfaces/estructuras';
import { MatDialog } from '@angular/material';
import { Planillas } from 'src/app/interfaces/planillas';
import { FuncionesService } from 'src/app/services/funciones.service';
@Component({
  selector: 'app-planillas-usuario',
  templateUrl: './planillas-usuario.component.html',
  styleUrls: ['./planillas-usuario.component.scss']
})
export class PlanillasUsuarioComponent implements OnInit {

  busqueda;
  public data: any[];
  public rowsOnPage = 5;
  item: Planillas = {
    idplanilla: null,
    idartista: null,
    pais: null,
    ciudad: null,
    fecha: null,
    nombre_evento: null,
    lugar_evento: null,
    responsable: null,
    telefono_responsable:null,
    direccion_responsable:null,
    vigencia: 1,
    creacion: 'now()',
    modificacion: null
  }

  idplanilla;
  constructor(public dialog: MatDialog,private fmysql: FmysqlService,private funcion:FuncionesService) { }


  ngOnInit() {
    this.idplanilla=this.funcion.getUsuarioLogueado().id;
    this.listar_planillas();
  }

  listar_planillas(){
    this.fmysql.listar_api(environment.api_planilla_x_usuario+this.idplanilla).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.data = res.data;
      this.data.map(fila=>fila.texto_buscable = fila.nombre + " "+fila.responsable)
    });
  }




  eliminar(id){
    this.fmysql.borrar(environment.api_planillas, id).subscribe(snap => {
      console.log(snap);
      let res: respuestaAPI = snap.json();
      if(res.ok){
        this.ngOnInit();
      }
    });
  }



}
