import { Component, OnInit } from '@angular/core';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { respuestaAPI } from 'src/app/interfaces/estructuras';
import { environment } from 'src/environments/environment';
import { FuncionesService } from 'src/app/services/funciones.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-dash-inicio',
  templateUrl: './dash-inicio.component.html',
  styleUrls: ['./dash-inicio.component.scss']
})
export class DashInicioComponent implements OnInit {
planillas;
usuarios;
artistas;
nivel;
planilla_user;
idusuario;
video;
videos=[];
  constructor(private fmysql:FmysqlService,private funcion:FuncionesService,public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.nivel=this.funcion.getUsuarioLogueado().nivel;
    this.idusuario=this.funcion.getUsuarioLogueado().id;
    this.listar_artistas();
    this.listar_planillas();
    this.listar_usuarios();
    this.listar_planillas_user();
    this.listar_videos();
  }


  listar_planillas(){
    this.fmysql.listar(environment.api_planillas).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.planillas = res.data.length;
    });
  }
  listar_planillas_user(){
    this.fmysql.buscar_campo(environment.api_planillas,"idusuario",this.idusuario).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.planilla_user = res.data.length;
    });
  }
  listar_usuarios(){
    this.fmysql.listar(environment.api_usuarios).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.usuarios = res.data.length;
    });
  }
  listar_artistas(){
    this.fmysql.listar(environment.api_artistas).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.artistas = res.data.length;
    });
  }
  listar_videos(){
    this.fmysql.listar(environment.configuracion.api_videos).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.videos = res.data;
      this.video=res.data.length;
    });
  }



}
