import { Component, OnInit } from '@angular/core';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { FuncionesService } from 'src/app/services/funciones.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { environment } from 'src/environments/environment';
import { respuestaAPI } from 'src/app/interfaces/estructuras';
import { FormVideosComponent } from 'src/app/modales/form-videos/form-videos.component';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.scss']
})
export class ConfiguracionComponent implements OnInit {


  videos = [];
  constructor(public dialog: MatDialog, private fmysql: FmysqlService, private funcion: FuncionesService, public sanitizer: DomSanitizer) { }



  ngOnInit() {
    this.listar_videos();
  }

  listar_videos() {
    this.fmysql.listar(environment.configuracion.api_videos).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.videos = res.data;
    });
  }

  modal_registrar_video(editar: boolean, item?: any) {
    //this.mostrarMsgBox(1);
    const dialogRef = this.dialog.open(FormVideosComponent, {
      panelClass: ['panel-modal', 'panel-modal-30'],
      data: { editar: editar, item: item }

    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listar_videos();
      }
    });
  }


  eliminar(id) {
    this.fmysql.borrar(environment.configuracion.api_videos, id).subscribe(snap => {
      console.log(snap);
      let res: respuestaAPI = snap.json();
      if (res.ok) {
        this.ngOnInit();
      }
    });
  }

}
