import { Component, OnInit } from '@angular/core';
import { Usuarios } from 'src/app/interfaces/usuarios';
import { formatoGuardar, respuestaAPI } from 'src/app/interfaces/estructuras';
import { environment } from 'src/environments/environment';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { ListasSistema } from 'src/app/interfaces/listas-sistemas';

@Component({
  selector: 'app-registro-usuarios',
  templateUrl: './registro-usuarios.component.html',
  styleUrls: ['./registro-usuarios.component.scss']
})
export class RegistroUsuariosComponent implements OnInit {
  item: Usuarios = {
    idusuario: null,
    ci: null,
    nombres: null,
    apaterno: null,
    amaterno: null,
    celular: null,
    correo: null,
    clave: null,
    nivel: 1,
    vigencia: 1,
    creacion: 'now()',
    modificacion: null
  }

  roles=new ListasSistema().tipo_ingresos;

  hide: boolean = true;
  //Validaciono de Formulario
  nombres = new FormControl([Validators.required, Validators.pattern('[a-zA-Z]*')]);
  //dni = new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('^(0|[1-9][0-9]*)$')]);
  ci = new FormControl('', [Validators.required]);
  apaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*$')]);
  amaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*$')]);
  correo=new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]);

  constructor(private fmysql: FmysqlService, private _router: Router, private snackBar: MatSnackBar) { }

  _email(requerido?: boolean): FormControl{
    let formato_email = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';
    let control = new FormControl('', [Validators.email, Validators.pattern(formato_email)]);
    if(requerido){
      control.setValidators([Validators.required, Validators.email, Validators.pattern(formato_email)]);
    }
    control.updateValueAndValidity();
    return control;
  }

  ngOnInit() {
    this.item.nivel=1;
  }


  guardar() {

    let item_guardar: formatoGuardar = {
      datos: [this.item.idusuario, this.item.ci, this.item.nombres, this.item.apaterno, this.item.amaterno, this.item.celular, this.item.correo, this.item.clave, this.item.nivel, this.item.vigencia, this.item.creacion, this.item.modificacion]
    }
    this.fmysql.registrar(environment.api_usuarios, item_guardar).subscribe(snap => {
      console.log(snap);
      let res: respuestaAPI = snap.json();

      if (res.ok) {
        this.snackBar.open('Registro de usuario', 'correcto!!!', {
          horizontalPosition: 'right', duration: 3000
        });
        this._router.navigate(["/dash/usuarios"]);
      } else {
        this.snackBar.open('Error', ' no se pudo registrar!!!', {
          horizontalPosition: 'right', duration: 3000
        });
      }
    });
  }

}
