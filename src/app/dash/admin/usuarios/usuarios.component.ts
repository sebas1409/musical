import { Component, OnInit } from '@angular/core';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { environment } from 'src/environments/environment';
import { respuestaAPI } from 'src/app/interfaces/estructuras';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {
  busqueda;
  public data: any[];
  public rowsOnPage = 5;
  constructor(private fmysql: FmysqlService) { }

  ngOnInit() {
    this.listar_usuarios();
  }

  listar_usuarios(){
    this.fmysql.listar(environment.api_usuarios).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.data = res.data;
      this.data.map(fila=>fila.texto_buscable = fila.nombres +" "+ fila.apaterno+ " "+fila.ci)
    });
  }


  eliminar(id){
    this.fmysql.borrar(environment.api_usuarios, id).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      if(res.ok){
        this.listar_usuarios();
      }
    });
  }

}
