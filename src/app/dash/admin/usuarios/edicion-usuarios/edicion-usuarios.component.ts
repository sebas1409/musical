import { Component, OnInit } from '@angular/core';
import { Usuarios } from 'src/app/interfaces/usuarios';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { formatoModificacion, respuestaAPI } from 'src/app/interfaces/estructuras';
import { environment } from 'src/environments/environment';
import { FormControl, Validators } from '@angular/forms';
import { ListasSistema } from 'src/app/interfaces/listas-sistemas';

@Component({
  selector: 'app-edicion-usuarios',
  templateUrl: '../registro-usuarios/registro-usuarios.component.html',
  styleUrls: ['../registro-usuarios/registro-usuarios.component.scss']
})
export class EdicionUsuariosComponent implements OnInit {
item:Usuarios={
    idusuario: null,
    ci: null,
    nombres: null,
    apaterno: null,
    amaterno: null,
    celular:null,
    correo: null,
    clave: null,
    nivel: null,
    vigencia: null,
    creacion: null,
    modificacion: null
  }

  roles=new ListasSistema().tipo_ingresos;

  hide:boolean=true;
  //Validaciono de Formulario
  nombres = new FormControl([Validators.required, Validators.pattern('[a-zA-Z]*')]);
  //dni = new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('^(0|[1-9][0-9]*)$')]);
  ci = new FormControl('', [Validators.required]);
  apaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*$')]);
  amaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*$')]);
  correo=new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]);

  constructor(private fmysql: FmysqlService,private _router:Router,private snackBar:MatSnackBar,private state:ActivatedRoute){}

  ngOnInit() {
    this.item.idusuario=this.state.snapshot.params.id;
    this.busqueda_usuario();
  }


  busqueda_usuario(){
    this.fmysql.buscar_id(environment.api_usuarios,this.item.idusuario).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.item = res.data[0];
    });
  }
  guardar() {

    let item_update: formatoModificacion = {
      idcampo: {nombre: "idusuario", valor: this.item.idusuario},
      campos: ["ci","nombres","apaterno","amaterno","celular","correo","clave","nivel","modificacion"],
      valores: [this.item.ci,this.item.nombres,this.item.apaterno,this.item.amaterno,this.item.celular,this.item.correo,this.item.clave,this.item.nivel,'now()']

    }
    this.fmysql.modificar(environment.api_usuarios, item_update).subscribe(snap => {
      console.log(snap);

      let res: respuestaAPI = snap.json();
    if (res.ok) {
      this.snackBar.open('Modificacion de usuario','correcto!!!', {
        horizontalPosition: 'right', duration: 3000
      });
      this._router.navigate(["/dash/usuarios"]);

    } else {
      this.snackBar.open('Error','  No se pudo modificar!!!', {
        horizontalPosition: 'right', duration: 3000
      });
    }
    });

}
}
