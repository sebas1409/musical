import { Component, OnInit, Input } from '@angular/core';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { environment } from 'src/environments/environment';
import { respuestaAPI } from 'src/app/interfaces/estructuras';
import { MatDialog } from '@angular/material';
import { FormCancionesComponent } from 'src/app/modales/form-canciones/form-canciones.component';

@Component({
  selector: 'app-canciones',
  templateUrl: './canciones.component.html',
  styleUrls: ['./canciones.component.scss']
})
export class CancionesComponent implements OnInit {
  busqueda;
  public data: any[];
  public rowsOnPage = 5;
  @Input() idartista:number;
  constructor(public dialog: MatDialog,private fmysql: FmysqlService) { }


  ngOnInit() {
    this.listar_canciones();
  }

  listar_canciones(){
    this.fmysql.buscar_campo(environment.canciones.api_canciones,"idartista",this.idartista).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.data = res.data;
      this.data.map(fila=>fila.texto_buscable = fila.nombre)
    });
  }

    modal_registrar_cancion(editar:boolean,item?:any) {
    //this.mostrarMsgBox(1);
    const dialogRef = this.dialog.open(FormCancionesComponent, {
      panelClass: ['panel-modal', 'panel-modal-30'],
      data:{editar:editar,item:item,idartista:this.idartista}

    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listar_canciones();
      }
    });
  }


  eliminar(id){
    this.fmysql.borrar(environment.canciones.api_canciones, id).subscribe(snap => {
      console.log(snap);
      let res: respuestaAPI = snap.json();
      if(res.ok){
        this.ngOnInit();
      }
    });
  }

}
