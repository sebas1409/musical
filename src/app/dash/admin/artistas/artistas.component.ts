import { Component, OnInit } from '@angular/core';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { environment } from 'src/environments/environment';
import { respuestaAPI } from 'src/app/interfaces/estructuras';

@Component({
  selector: 'app-artistas',
  templateUrl: './artistas.component.html',
  styleUrls: ['./artistas.component.scss']
})
export class ArtistasComponent implements OnInit {

  busqueda;
  public data: any[];
  public rowsOnPage = 5;

  constructor(private fmysql: FmysqlService) { }

  ngOnInit() {
    this.listar_artistas();
  }

  listar_artistas(){
    this.fmysql.listar(environment.api_artistas).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.data = res.data;
      this.data.map(fila=>fila.texto_buscable = fila.genero +" "+ fila.nombre)
    });
  }


  eliminar(id){
    this.fmysql.borrar(environment.api_artistas, id).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      if(res.ok){
        this.listar_artistas();
      }
    });
  }

}
