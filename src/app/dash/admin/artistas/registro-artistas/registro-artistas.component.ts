import { Component, OnInit } from '@angular/core';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { formatoGuardar, respuestaAPI } from 'src/app/interfaces/estructuras';
import { environment } from 'src/environments/environment';
import { Artistas } from 'src/app/interfaces/artistas';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-registro-artistas',
  templateUrl: './registro-artistas.component.html',
  styleUrls: ['./registro-artistas.component.scss']
})
export class RegistroArtistasComponent implements OnInit {

  item:Artistas={
    idartista: null,
    genero: null,
    nombre: null,
    celular:null,
    correo: null,
    vigencia: 1,
    creacion: 'now()',
    modificacion: null
  }
 //Validaciono de Formulario
 nombre = new FormControl([Validators.required, Validators.pattern('^([a-zA-Z]+)$')]);
 // apaterno = new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('^(0|[1-9][0-9]*)$')]);
 genero = new FormControl([Validators.required, Validators.pattern('^([a-zA-Z]+)$')]);
 correo=new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]);

  constructor(private fmysql: FmysqlService,private _router:Router,private snackBar:MatSnackBar){}

  ngOnInit() {
  }


  guardar() {

    let item_guardar: formatoGuardar = {
      datos: [this.item.idartista, this.item.genero,this.item.nombre,this.item.celular,this.item.correo,this.item.vigencia,this.item.creacion,this.item.modificacion]
    }
    this.fmysql.registrar(environment.api_artistas, item_guardar).subscribe(snap => {
      console.log(snap);
      let res: respuestaAPI = snap.json();

      if (res.ok) {
        this.snackBar.open('Registro de artista','correcto!!!', {
          horizontalPosition: 'right', duration: 3000
        });
        this._router.navigate(["/dash/artistas"]);
      } else {
        this.snackBar.open('Error',' no se pudo registrar!!!', {
          horizontalPosition: 'right', duration: 3000
        });
      }
    });
}

}
