import { Component, OnInit } from '@angular/core';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Artistas } from 'src/app/interfaces/artistas';
import { environment } from 'src/environments/environment';
import { respuestaAPI, formatoModificacion } from 'src/app/interfaces/estructuras';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edicion-artistas',
  templateUrl: '../registro-artistas/registro-artistas.component.html',
})
export class EdicionArtistasComponent implements OnInit {

  item:Artistas={
    idartista: null,
    genero: null,
    nombre: null,
    celular:null,
    correo: null,
    vigencia: 1,
    creacion: null,
    modificacion: null
  }
 //Validaciono de Formulario
 nombre = new FormControl( [Validators.required, Validators.pattern('^([a-zA-Z]+)$')]);
 // apaterno = new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('^(0|[1-9][0-9]*)$')]);
 genero = new FormControl([Validators.required, Validators.pattern('^([a-zA-Z]+)$')]);
 correo=new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]);


  constructor(private fmysql: FmysqlService,private _router:Router,private snackBar:MatSnackBar,private state:ActivatedRoute){}

  ngOnInit() {
    this.item.idartista=this.state.snapshot.params.id;
    this.busqueda_artista();
  }


  busqueda_artista(){
    this.fmysql.buscar_id(environment.api_artistas,this.item.idartista).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.item = res.data[0];
    });
  }
  guardar() {

    let item_update: formatoModificacion = {
      idcampo: {nombre: "idartista", valor: this.item.idartista},
      campos: ["genero","nombre","celular","correo","modificacion"],
      valores: [this.item.genero,this.item.nombre,this.item.celular,this.item.correo,'now()']

    }
    this.fmysql.modificar(environment.api_artistas, item_update).subscribe(snap => {
      let res: respuestaAPI = snap.json();
    console.log(res);
    if (res.ok) {
      this.snackBar.open('Modificacion de artista','correcto!!!', {
        horizontalPosition: 'right', duration: 3000
      });
      this._router.navigate(["/dash/artistas"]);

    } else {
      this.snackBar.open('Error','  No se pudo modificar!!!', {
        horizontalPosition: 'right', duration: 3000
      });
    }
    });

}

}
