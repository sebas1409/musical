import { Component, OnInit } from '@angular/core';
import { Http} from '@angular/http';
import { Router } from '@angular/router';
import { respuestaAPI } from 'src/app/interfaces/estructuras';
import { FuncionesService } from 'src/app/services/funciones.service';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {



  usuario_logueado: string;
  nivel:number=Number(this.funcion.getUsuarioLogueado().nivel);


  constructor(private _router: Router,private http: Http,private funcion:FuncionesService,private fmysql:FmysqlService) {}

  ngOnInit() {
    this.listar_admin();
  }


  miPerfil(){
    this._router.navigateByUrl("dash/admin/mi-perfil");
  }

  salir(){
    localStorage.removeItem("musical_id");
    localStorage.removeItem("musical_nivel");
    this._router.navigate(["/acceso"]);

  }


  listar_admin(){

    let idadmin=localStorage.getItem("musical_id");
    this.fmysql.buscar_id(environment.api_usuarios,Number(idadmin)).subscribe(snap=>{
      let res: respuestaAPI = snap.json();
      this.usuario_logueado=res.data[0].nombres;

    });

  }

}
