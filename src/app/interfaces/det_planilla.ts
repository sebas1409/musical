export interface DetPlanilla {
	iddetplanilla?: number;
	idplanilla?: number;
	tema?: string;
  autor?: string;
  veces?: number;
	otros?: string;
	vigencia?: number;

}
