export interface Artistas {
  idartista?: number;
	genero?: string;
	nombre?: string;
  celular?:string;
  correo?: string;
	vigencia?: number;
	creacion?: string;
	modificacion?: string;
}
