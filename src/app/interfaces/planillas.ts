export interface Planillas {
  idplanilla?: number; // int(11)
  idusuario?:number | string;
  idartista?:number;
  nic?:string;
  pais?:string;
  ciudad?:string;
  fecha?:string;
  nombre_evento?:string;
  lugar_evento?:string;
  responsable?:string;
  telefono_responsable?:string;
  direccion_responsable?:string;
  descripcion?:string;
	vigencia?: number; // tinyint(1)
	creacion?: string; // datetime
	modificacion?: string; // datetime
}
