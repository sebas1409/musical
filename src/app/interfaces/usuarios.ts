export interface Usuarios {
	idusuario?: number;
	ci?: string;
	nombres?: string;
  apaterno?: string;
  amaterno?: string;
  celular?:string;
  correo?: string;
  clave?: string;
	nivel?: number;
	vigencia?: number;
	creacion?: string;
	modificacion?: string;
}
