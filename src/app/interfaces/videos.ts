export interface Videos {
  idvideo?: number;
  url_video?: string;
  descripcion?: string;
  vigencia?: number;
}
