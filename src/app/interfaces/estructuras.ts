// tslint:disable

export interface formatoModificacion {
  idcampo: {nombre: string, valor: number},
  campos: string[],
  valores: any[]
};

export interface formatoEjecutar {
  sql: string,
  metodo: "put"|"get"
}

export interface formatoGuardar {
  datos: any[]
}

export interface respuestaAPI{
  ok?: boolean,
  error?: boolean,
  sql?: string,
  data?: any[],
  msg?: string,
  id?: number,
  nombre?: string
}

export interface opcionesEnLista {
  codigo: any,
  nombre: any,
  alias?: string
}

export interface mesaSelConPedido{
  idmesa?: number, 
  nombre?: string, 
  cantidad?: number
}