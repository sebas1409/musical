export interface Canciones {
	idcancion?: number; // int(11)
	idartista?: string; // int(11)
	nombre?: string; // varchar(255)
	vigencia?: boolean; // tinynt(1)
	creacion?: string; // datetime
	modificacion?: string; // datetime
}
