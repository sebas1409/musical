import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { formatoGuardar, respuestaAPI, formatoModificacion } from 'src/app/interfaces/estructuras';
import { environment } from 'src/environments/environment';
import { Canciones } from 'src/app/interfaces/canciones';
@Component({
  selector: 'app-form-canciones',
  templateUrl: './form-canciones.component.html',
  styleUrls: ['./form-canciones.component.scss']
})
export class FormCancionesComponent implements OnInit {


  item:Canciones={
    idcancion:null,
    idartista: null,
    nombre:null,
    vigencia:null,
    creacion:null,
    modificacion:null
  }

  titulo: string;
  editable: boolean;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,public dialogRef: MatDialogRef<FormCancionesComponent>,private fmysql: FmysqlService
  //,private alerta:FuncionesSweetalert2Service
  ) { }


  ngOnInit() {

    console.log(this.data);


    this.editable = this.data.editar;

    if (this.editable == true) {
      this.item = this.data.item;
      this.titulo = "Editar Cancion";
    } else {
      this.item.idartista=this.data.idartista;
      this.titulo = "Registrar Cancion";

    }
    //this.listarItem();
    //this.listar_tipo();

  }

  validar_accion() {
    if (this.editable) {
      console.log("Modificar");
      this.modificar();
    } else {
      console.log("Guardar");
      this.guardar();
    }
  }


  cancelar() {
    this.dialogRef.close(false);
  }
  guardar() {

      let item_guardar: formatoGuardar = {
        datos: [this.item.idcancion, this.item.idartista,this.item.nombre,1, 'now()', null]
      }
      this.fmysql.registrar(environment.canciones.api_canciones, item_guardar).subscribe(snap => {
        console.log(snap);
        let res: respuestaAPI = snap.json();

        if (res.ok) {
          //this.alerta.registrar();
          this.dialogRef.close(true);
        } else {
          this.dialogRef.close(false);
        }
      });
  }

  modificar() {

      let item_update: formatoModificacion = {
        idcampo: {nombre: "idcancion", valor: this.item.idcancion},
        campos: ["nombre","modificacion"],
        valores: [this.item.nombre,'now()']
      }
      this.fmysql.modificar(environment.canciones.api_canciones, item_update).subscribe(snap => {
        let res: respuestaAPI = snap.json();
      console.log(res);
      if (res.ok) {
       // this.alerta.actualizar();
        this.dialogRef.close(true);
      } else {
        this.dialogRef.close(false);
      }
      });

  }

}
