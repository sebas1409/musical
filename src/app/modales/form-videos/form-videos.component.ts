import { Component, OnInit, Inject } from '@angular/core';
import { Videos } from 'src/app/interfaces/videos';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { formatoModificacion, respuestaAPI, formatoGuardar } from 'src/app/interfaces/estructuras';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-form-videos',
  templateUrl: './form-videos.component.html',
  styleUrls: ['./form-videos.component.scss']
})
export class FormVideosComponent implements OnInit {
item:Videos={
  idvideo:null,
  url_video:null,
  descripcion:null,
  vigencia:null
}
titulo: string;
editable: boolean;
constructor(@Inject(MAT_DIALOG_DATA) public data: any,public dialogRef: MatDialogRef<FormVideosComponent>,private fmysql: FmysqlService
) { }

ngOnInit() {

  console.log(this.data);


  this.editable = this.data.editar;

  if (this.editable == true) {
    this.item = this.data.item;
    this.titulo = "Editar Videos";
  } else {
    this.titulo = "Registrar Videos";

  }
  //this.listarItem();
  //this.listar_tipo();

}

validar_accion() {
  if (this.editable) {
    console.log("Modificar");
    this.modificar();
  } else {
    console.log("Guardar");
    this.guardar();
  }
}


cancelar() {
  this.dialogRef.close(false);
}
guardar() {

    let item_guardar: formatoGuardar = {
      datos: [this.item.idvideo, this.item.url_video,this.item.descripcion,1]
    }
    this.fmysql.registrar(environment.configuracion.api_videos, item_guardar).subscribe(snap => {
      console.log(snap);
      let res: respuestaAPI = snap.json();

      if (res.ok) {
        //this.alerta.registrar();
        this.dialogRef.close(true);
      } else {
        this.dialogRef.close(false);
      }
    });
}

modificar() {

    let item_update: formatoModificacion = {
      idcampo: {nombre: "idvideo", valor: this.item.idvideo},
      campos: ["url_video","descripcion"],
      valores: [this.item.url_video,this.item.descripcion]
    }
    this.fmysql.modificar(environment.configuracion.api_videos, item_update).subscribe(snap => {
      let res: respuestaAPI = snap.json();
    console.log(res);
    if (res.ok) {
     // this.alerta.actualizar();
      this.dialogRef.close(true);
    } else {
      this.dialogRef.close(false);
    }
    });

}

}
