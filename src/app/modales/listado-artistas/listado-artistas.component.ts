import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { environment } from 'src/environments/environment';
import { respuestaAPI } from 'src/app/interfaces/estructuras';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-listado-artistas',
  templateUrl: './listado-artistas.component.html',
  styleUrls: ['./listado-artistas.component.scss']
})
export class ListadoArtistasComponent implements OnInit {

  busqueda;
  public data: any[];
  public rowsOnPage = 5;

  constructor(public dialog: MatDialog,private fmysql: FmysqlService,private cdRef: ChangeDetectorRef,private dialogRef: MatDialogRef<ListadoArtistasComponent>) { }

  ngOnInit() {
    this.listar_artistas();
  }

  listar_artistas(){
    this.fmysql.listar(environment.api_artistas).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.data = res.data;
      this.data.map(fila=>fila.texto_buscable = fila.genero +" "+ fila.nombre)
    });
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  seleccionar(item){
    this.dialogRef.close(item);
  }
}
