import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatIconModule, MatButtonModule, MatAutocompleteModule, MatSelectModule, MatDialogModule, MatMenuModule } from '@angular/material';
import { FormCancionesComponent } from './form-canciones/form-canciones.component';
import { ListadoDeclaracionesComponent } from './listado-declaraciones/listado-declaraciones.component';
import { DataTableModule } from 'angular-6-datatable';
import { MisPipesModule } from '../pipes/mis-pipes.module';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ListadoArtistasComponent } from './listado-artistas/listado-artistas.component';
import { FormVideosComponent } from './form-videos/form-videos.component';

@NgModule({
  imports: [
    CommonModule,FormsModule, MatInputModule,MatMenuModule,
    MatIconModule, MatButtonModule, ReactiveFormsModule, MatAutocompleteModule,
    MatSelectModule, MatDialogModule,DataTableModule,MisPipesModule,SharedModule,RouterModule,FormsModule
  ],
  declarations: [FormCancionesComponent, ListadoDeclaracionesComponent, ListadoArtistasComponent, FormVideosComponent],
  exports:[FormCancionesComponent,ListadoDeclaracionesComponent,ListadoArtistasComponent,FormVideosComponent],
  entryComponents:[FormCancionesComponent,ListadoDeclaracionesComponent,ListadoArtistasComponent,FormVideosComponent]
})
export class MisModalesModule { }
