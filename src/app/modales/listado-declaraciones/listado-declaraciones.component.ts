import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FmysqlService } from 'src/app/services/fmysql.service';
import { environment } from 'src/environments/environment';
import { respuestaAPI } from 'src/app/interfaces/estructuras';

@Component({
  selector: 'app-listado-declaraciones',
  templateUrl: './listado-declaraciones.component.html',
  styleUrls: ['./listado-declaraciones.component.scss']
})
export class ListadoDeclaracionesComponent implements OnInit {
  busqueda;
  public data: any[];
  public rowsOnPage = 5;

  constructor(public dialog: MatDialog,private fmysql: FmysqlService,private cdRef: ChangeDetectorRef,private dialogRef: MatDialogRef<ListadoDeclaracionesComponent>) { }

  ngOnInit() {
    this.listar_declaraciones();
  }

  listar_declaraciones(){
    this.fmysql.listar_api(environment.canciones.api_declaraciones).subscribe(snap => {
      let res: respuestaAPI = snap.json();
      this.data = res.data;
      this.data.map(fila=>fila.texto_buscable = fila.nombre+" "+fila.autor)
    });
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  seleccionar(item){
    this.dialogRef.close(item);
  }
}
