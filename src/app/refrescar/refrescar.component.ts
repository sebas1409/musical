import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-refrescar',
  templateUrl: './refrescar.component.html',
  styleUrls: ['./refrescar.component.scss']
})
export class RefrescarComponent implements OnInit {

  constructor(private router:Router, private state:ActivatedRoute) { }

  ngOnInit() {

    let url = this.state.snapshot.params.url;
    console.log("URL Anterior: " + url);
   // url = url.replace(new RegExp('-', 'g'), '/');
    console.log("URL Actual: " + url);
    this.router.navigate([url]);
    //this.router.navigate(["/dash/"]);
  }

}
