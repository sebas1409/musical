import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefrescarComponent } from './refrescar.component';

describe('RefrescarComponent', () => {
  let component: RefrescarComponent;
  let fixture: ComponentFixture<RefrescarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefrescarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefrescarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
