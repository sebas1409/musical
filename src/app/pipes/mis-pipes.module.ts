import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RellenarPipe } from './rellenar.pipe';
import { FiltroPipe } from './filtro.pipe';
import { TruncatePipe } from './truncate.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FiltroPipe,RellenarPipe, TruncatePipe],
  exports:[FiltroPipe,RellenarPipe,TruncatePipe]
})
export class MisPipesModule { }
