// tslint:disable
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'titulo-seccion',
  templateUrl: './titulo-seccion.component.html',
  styleUrls: ['./titulo-seccion.component.scss']
})
export class TituloSeccionComponent implements OnInit {

  @Input() titulo: string;
  @Input() anterior: string;
  
  constructor() { }

  ngOnInit() {
  }

}
