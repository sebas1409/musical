import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';
import { MatIconModule } from '@angular/material/icon';

import { TituloSeccionComponent } from './titulo-seccion/titulo-seccion.component';

@NgModule({
  imports: [
    CommonModule, RouterModule, MatSlideToggleModule, FormsModule, ReactiveFormsModule, MatStepperModule, MatIconModule
  ],
  declarations: [TituloSeccionComponent, ],
  exports: [TituloSeccionComponent, ],
  entryComponents: []
})
export class SharedModule { }
