import { appInitializerFactory } from "@angular/platform-browser/src/browser/server-transition";

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  api_login:'http://localhost/apimusical/api/login/',

  api_usuarios:'http://localhost/apimusical/api/usuarios/',
  api_artistas:'http://localhost/apimusical/api/artistas/',
  canciones:{
    api_canciones:'http://localhost/apimusical/api/canciones/',
    api_declaraciones:'http://localhost/apimusical/api/canciones/declaraciones/'
  },
  api_planillas:'http://localhost/apimusical/api/planillas/',

  api_planilla_x_usuario:'http://localhost/apimusical/api/planillas/planilla_x_usuario/',

  api_maxplanilla: 'http://localhost/apimusical/api/planillas/maxplanilla/',

  api_det_planilla:'http://localhost/apimusical/api/det_planilla/',

  configuracion:{
    api_videos:'http://localhost/apimusical/api/videos/'
  }





};




