export const environment = {
  production: true,


  api_login:'https://gestruc.000webhostapp.com/apimusical/api/login/',

  api_usuarios:'https://gestruc.000webhostapp.com/apimusical/api/usuarios/',
  api_artistas:'https://gestruc.000webhostapp.com/apimusical/api/artistas/',
  canciones:{
    api_canciones:'https://gestruc.000webhostapp.com/apimusical/api/canciones/',
    api_declaraciones:'https://gestruc.000webhostapp.com/apimusical/api/canciones/declaraciones/'
  },
  api_planillas:'https://gestruc.000webhostapp.com/apimusical/api/planillas/',
  api_planilla_x_usuario:'https://gestruc.000webhostapp.com/apimusical/api/planillas/planilla_x_usuario/',

  api_maxplanilla: 'https://gestruc.000webhostapp.com/apimusical/api/planillas/maxplanilla/',

  api_det_planilla:'https://gestruc.000webhostapp.com/apimusical/api/det_planilla/',

  configuracion:{
    api_videos:'https://gestruc.000webhostapp.com/apimusical/api/videos/'
  }
};
